"""Design the similog Dialog, so as to parametrize the consensus computation algorithm, using FAMSA
"""

import os
import logging
import subprocess

import pandas as pd

from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt import uic

from albion_similog import well_correlation


logger = logging.getLogger(__name__)


def check_similog_dependency():
    """Check if the albion_similog Python dependency is installed on the computer."""
    try:
        import albion_similog

        logger.debug("Version of Similog: %s", albion_similog.__version__)
    except ModuleNotFoundError:
        return False
    return True


def check_famsa_dependency():
    """Check if the FAMSA dependency is installed on the computer."""
    try:
        subprocess.call(["famsa"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except FileNotFoundError:  # No famsa file in the current directory
        return False
    except PermissionError:  # A famsa file that is not executable
        return False
    except OSError:  # The famsa file is not really the expected program
        return False
    return True


class SimilogDialog(QDialog):
    """Dialog for setting the similog algorithm"""

    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "similog.ui"), self)
        self.__project = project

    def exec(self):
        self._dialog_button.setEnabled(False)
        self._similog_info.setText(
            "Click on 'Compute' button to run similog procedure..."
        )
        self._similog_progress.setValue(0)
        min_depth = min(self._depth_min.value(), self._depth_max.value())
        max_depth = max(self._depth_min.value(), self._depth_max.value())
        min_value = min(self._value_min.value(), self._value_max.value())
        max_value = max(self._value_min.value(), self._value_max.value())
        logger.info("Compute Similog resistivity...")
        with self.__project.connect() as con:
            # Read the data into the database
            logger.info("Read the resistivity data...")
            self._similog_info.setText("Read the resistivity data...")
            self._similog_progress.setValue(5)
            df = pd.read_sql(
                f"SELECT hole_id, from_, rho FROM {self._table.text()}", con
            )
            # Compute similog outputs
            df.reset_index(drop=True, inplace=True)
            logger.info("Create Similog object...")
            self._similog_info.setText("Create Similog object...")
            self._similog_progress.setValue(10)
            self.wcorr = well_correlation.WellCorrelation(
                df,
                match_column="rho",
                depth_column="from_",
                well_column="hole_id",
                min_seg=int(self._min_seg.value()),
                pelt_sup=self._pelt_sup.value(),
                depth_min=min_depth,
                depth_max=max_depth,
                value_min=min_value,
                value_max=max_value,
                log_normalize=self._log_normalize.isChecked(),
                lr_normalize=self._transcribe_regressed_values.isChecked(),
                segmentize_with_pelt=self._segmentize_with_pelt.isChecked(),
            )
            logger.info("Compute Similog outputs...")
            self._similog_info.setText("Compute Similog outputs...")
            self._similog_progress.setValue(50)
            self.wcorr.run()
        self._similog_info.setText("Similog computation is OK!")
        self._similog_progress.setValue(100)
        self._dialog_button.setEnabled(True)

    def accept(self):
        logger.info("Accept Similog computation, waiting for insertion in base...")
        self.__project.insert_similog_consensus()
        self.__project.insert_similog_markers(self.wcorr.markers)
        self.close()
